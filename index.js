const express = require("express");
var bodyParser = require("body-parser");
var cors = require('cors')

const palaces = require("./controllers/palaces");
const pole = require("./controllers/poles");
const validateRoute = require("./controllers/validate");
const app = express();
const mongoose = require("mongoose");
app.use(cors())
//mongoose.connect('mongodb://localhost/pole',{ useNewUrlParser: true })
mongoose.connect("mongodb://pole:pole123@ds213118.mlab.com:13118/pole", {useNewUrlParser: true})
  .then(() => console.log("Connected to db successfully..."))
  .catch(() => console.log("Failed to connect db..."));
  
  app.use(bodyParser.json());
  
  app.use(bodyParser.urlencoded({ extended: true })); 
  app.use(function(req, res, next) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Credentials", true);
    res.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
    res.setHeader("Access-Control-Max-Age", "3600");
    res.setHeader("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With, auth");
    
    /*res.header("Access-Control-Allow-Origin", "*");
    res.header(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept",
      );
      */ 
      
      next();
    });
app.use(validateRoute);
//app.use(upload.array('array')); 
app.use("/palace", palaces);
app.use("/pole", pole);

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => console.log(`Server is Listening at Port : ${PORT}`));
