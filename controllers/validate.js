var jwt = require('jsonwebtoken');

const validateRoutes = [
    "/palace/addstate",
    "/palace/adddist",
    "/palace/deletedist",
    "/palace/deletestate",
    "/palace/addparty",
    "/palace/editparty",
    "/pole/add",
    "/pole/deletepole",
    "/pole/editpole",
    "/pole/checkuser",
    "/pole/getqueries"
]
function validate(req, res, next) {
    try{
        let matchedValid  = validateRoutes.some(item =>req.originalUrl.includes(item))
        if(!matchedValid){
            next();
            return;
        }
        if(!req.headers.auth){
            res.status(401).send({status:false,msg:'Not valid User'})
            return;
        }
        jwt.verify(req.headers.auth, 'yogy')
        next();
    } catch (err){
        res.status(401).send({status:false,msg:'Authentication failed',err:err})
    }
}
module.exports = validate;