const express = require("express");
const router = express.Router();
const Pole = require('../models/poles')
const Palaces = require('../models/palaces')
const Parties = require('../models/parties')
const Queries = require('../models/queries')
const publicIp = require('public-ip');
const requestIp = require('request-ip');
var jwt = require('jsonwebtoken');



router.get('/', (req, res) => {
    res.send('fisrt pole request')
})
router.post('/login', async (req, res) => {
    try {
        uname = 'dharm', pass = 'poll';
        if (!(req.body.username == uname && req.body.password == pass)) {
            res.send({
                status: false,
                msg: 'login details are not correct.'
            })
            return;
        }
        let token = jwt.sign({
            id: uname
        }, 'yogy', {
            expiresIn: 86400 // expires in 24 hours
        });
        res.send({
            status: true,
            msg: 'login successfully...',
            token: token
        })
        //jwt.verify(header, 'yogy')
    } catch (err) {
        res.send({
            status: false,
            msg: err
        })

    }
})
router.post('/checkuser', async (req, res) => {
    try {

        let rs = jwt.verify(req.body.token, 'yogy')
        rs.status = true,
            rs.msg = 'success'
        res.send(rs)
    } catch (err) {
        res.send({
            status: false,
            msg: err
        })
    }
})
router.post('/add', async (req, res) => {
    try {
        let options = [];
        if (!req.body.use_custom_options) {
            const parties = await Parties.find({
                status: 'active'
            })
            options = parties.map(item => {
                return {
                    name: item.name,
                    image: item.image
                }
            })
        } else {
            options = req.body.custom_options.split(",").map(item => {
                return {
                    name: item
                }
            });
        }
        let totalvote = options.map(item => {
            return {
                "party": item.name,
                "count": 0
            }
        })
        let pole = new Pole({
            que: req.body.pole_que,
            options: options,
            totalvote: totalvote,
            status: req.body.status
        })
        let status = await pole.save();
        res.send({
            status: true,
            msg: 'Question Added Successfully...'
        })
    } catch (err) {
        res.status(400).send(err)
    }
})
router.get('/deletepole/:poleId', async (req, res) => {
    try {
        let con = {
            "_id": req.params.poleId,
        }
        let rs = await Pole.deleteOne(con)
        rs.status = true;
        rs.msg = 'Poll Question Deleted.'
        res.send(rs)
    } catch (err) {
        err.status = false;
        err.msg = 'Poll Question not Deleted'
        res.status(422).send(err)
    }
})
router.post('/editpole/:poleId', async (req, res) => {
    try {
        let con = {
            "_id": req.params.poleId,
        }
        let updateData = {};
        if (req.body.status) {
            updateData.status = req.body.status
        }
        let rs = await Pole.update(con, updateData)
        rs.status = true;
        rs.msg = 'Poll Question Updated.'
        res.send(rs)
    } catch (err) {
        err.status = false;
        err.msg = 'Poll Question not Deleted'
        res.status(422).send(err)
    }
})
router.post('/makevote/:poleId', async (req, res) => {
    try {
        let poleId = req.params.poleId;
        let random = Math.round(Math.random() * (+4 - +0) + +0);
        let successMsgs = [
            "Wow ! You made Your Vote",
            "Your Vote is done.",
            "Thanks for Vote",
            "Hmmm...Vote Done.",
            "Ok...Your Vote added."
        ];
        let errorMsgs = [
            "Opps! You Voted Already.",
            "You can't vote! You done.",
            "Already Voted for this Question,",
            "No, You can't Vote multiple times",
            "Ahh! You already done with this Question"
        ];
        let successRes = {
            status: true,
            msg: successMsgs[random]
        }
        let errorRes = {
            status: false,
            msg: errorMsgs[random]
        }
        //let voterIp = await publicIp.v4();
        //let voterIp = requestIp.getClientIp(req);
        let voterIp = req.headers['x-forwarded-for'] || req.connection.remoteAddress
        let notVoted = await Pole.findOneAndUpdate({
            "_id": poleId,
            voters: {
                $nin: [voterIp]
            }
        }, {
            $push: {
                "voters": voterIp
            }
        }, {
            new: true
        })
        if (!notVoted) {
            res.send(errorRes)
            return;
        }
        if (!req.body.vote_to) {
            res.send({
                status: false,
                msg: "Opps! You didn't select any option."
            })
            return;
        }
        const state = req.body.voter_state
        const dist = req.body.voter_dist
        const party = req.body.vote_to

        await Pole.update({
            "_id": poleId,
            "totalvote.party": party
        }, {
            $inc: {
                "totalvote.$.count": 1
            }
        })
        let con1 = {
            "_id": poleId,
        }
        let elematchcon = {
            vote: {
                $elemMatch: {
                    "party": party,
                    "state": state,
                    "dist": dist,
                }
            }
        };
        let data = await Pole.find(con1, elematchcon, {
            "vote.$": 1
        })
        if (data && data[0] && data[0].vote && data[0].vote[0] && data[0].vote[0]._id) {
            con1["vote._id"] = data[0].vote[0]._id;
            let op = await Pole.findOneAndUpdate(con1, {
                $inc: {
                    "vote.$.count": 1
                },
            }, {
                new: true
            })
            res.send(successRes)
            return;
        }
        let insertDone = await Pole.findOneAndUpdate(con1, {
            $push: {
                "vote": {
                    dist: dist,
                    state: state,
                    count: 1,
                    party: party
                }
            }
        }, {
            new: true
        })
        res.send(successRes);
        return;
    } catch (err) {
        res.status(400).send(err)
    }
})

router.get('/getpole', async (req, res) => {
    let voterIp = req.headers['x-forwarded-for'] || req.connection.remoteAddress
    let pole = await Pole.find({
        status: "active"
    }, '', {
        lean: true
    })

    pole.forEach(item => {
        item.voteStatus = (item.voters.indexOf(voterIp) == -1) ? 'Vote' : 'Voted'
    });
    res.send(pole);

})
router.get('/getallpoles', async (req, res) => {
    let voterIp = req.headers['x-forwarded-for'] || req.connection.remoteAddress
    let pole = await Pole.find({}, '', {
        lean: true
    })

    pole.forEach(item => {
        item.voteStatus = (item.voters.indexOf(voterIp) == -1) ? 'Vote' : 'Voted'
    });
    res.send(pole);

})
router.post('/submitqueries', async (req, res) => {
    try {

        let reqData = req.body;
        reqData.date = new Date();
        
        let query = new Queries(reqData)
        let st = await query.save();
        
        res.send({
            status: true,
            msg: 'Query submitted.'
        })
    } catch (err) {
        err.status = false,
        err.msg = 'Query not submitted.'
        res.send(err)
    }
})
router.get('/getqueries',async (req,res) => {
    let queries = await Queries.find().sort({date:-1})
    res.send(queries)
})
module.exports = router;