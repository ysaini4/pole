const express = require("express");
const router = express.Router();
const Palaces = require('../models/palaces')
const Parties = require('../models/parties')
var cloudinary = require('cloudinary');
const Datauri = require('datauri');
const path = require('path');
var multer = require('multer');
const dUri = new Datauri();
//var upload = multer({ dest: 'uploads/' });
const storage = multer.memoryStorage();
const multerUploads = multer({
  storage
}).single("image");
cloudinary.config({
  cloud_name: 'ysaini4',
  api_key: '418661371142981',
  api_secret: 'Jb6PuRIoHUpOC6S1yiSd2R_Uc94'
});
router.get("/list", async (req, res) => {
  let rs = await Palaces.find().sort({"state.name": 'asc'});
  res.send(rs);

});

router.post("/addstate", async (req, res) => {
  try {
    let stateData = req.body.state.name.split(",")
    const pl = stateData.length;
    for (let i = 0; i < pl; i++) {
      let state = stateData[i]
      if (state) {
        let query = {
            "state.name": state
          },
          update = {
            "state.name": state
          },
          options = {
            upsert: true,
            new: true,
            setDefaultsOnInsert: true
          };
        await Palaces.update(query, update, options)
      }
    };
    res.send({
      status: true,
      'msg': 'state updated'
    });
  } catch (err) {
    res.send(err)
  }
});
router.post('/adddist/:stateId', async (req, res) => {
  try {
    let distData = req.body.dist.split(",");
    const pl = distData.length;
    for (let i = 0; i < pl; i++) {
      let dist = distData[i]
      if (dist) {
        let rs = await Palaces.update({
          "_id": req.params.stateId,
          "state.dist.name": {
            $ne: dist
          }
        }, {
          $push: {
            "state.dist": [{
              name: dist
            }]
          }
        }, {
          new: true
        })
      }
    }
    res.send({
      status: true,
      "msg": "dist updated"
    })
  } catch (err) {
    res.send(err)
  }
});
router.get('/deletedist/:stateId/:distId', async (req, res) => {
  try {

    let con = {
      "_id": req.params.stateId,
    }
    let upSt = {
      "$pull": {
        "state.dist": {
          "_id": req.params.distId
        }
      }
    }
    let rs = await Palaces.update(con, upSt)
    rs.status = true;
    rs.msg = 'District Deleted.'
    res.send(rs)
  } catch (err) {
    err.status = false;
    err.msg = 'District not deleted'
    res.status(422).send(err)
  }
})
router.get('/deletestate/:stateId', async (req, res) => {
  try {
    let con = {
      "_id": req.params.stateId,
    }
    let rs = await Palaces.deleteOne(con)
    rs.status = true;
    rs.msg = 'State Deleted.'
    res.send(rs)
  } catch (err) {
    err.status = false;
    err.msg = 'State not deleted'
    res.status(422).send(err)
  }
})
router.post('/addparty', async (req, res) => {
  try {

    let partyData = req.body.name.split(",")
    const pl = partyData.length;

    for (let i = 0; i < pl; i++) {
      let party = partyData[i]

      if (party) {
        let partyRef = new Parties({
          name: party,
        })
        await partyRef.save();
      }

    }

    res.send({
      status: true,
      msg: "Party updated"
    })

  } catch (err) {
    err.status = false;
    err.msg = 'party not updated'
    res.status(422).send(err)
  }
})
router.post('/editparty/:partyId', multerUploads, async (req, res) => {
  try {
    const con = {
      "_id": req.params.partyId
    }

    let updateData = {}
    if (req.body.name) {
      updateData.name = req.body.name
    }
    if (req.body.status) {
      updateData.status = req.body.status
    }
    let cont;
    if (req.file) {
      const dataUri = req => dUri.format(path.extname(req.file.originalname).toString(), req.file.buffer);
      cont = dataUri(req).content
    }
    if (req.body.image) {
      cont = req.body.image
    }
    if (cont) {
      let folder;
      if (req.body.folder) {
        folder = req.body.folder
      } else {
        folder = 'poleParties';
      }
      let cloudRes = await cloudinary.v2.uploader.upload(cont, {
        folder: folder
      });
      updateData.image = cloudRes.secure_url
    }
    let rl = await Parties.update(con, updateData)
    rl.status = true;
    rl.msg = 'Party detail updated'
    res.send(rl)
  } catch (err) {
    err.status = false;
    err.msg = 'party not updated'
    res.status(422).send(err)
  }
})
router.get('/partylist', async (req, res) => {
  try {
    let partyList = await Parties.find();
    res.send(partyList)
  } catch (err) {
    err.status = false;
    res.status(422).send(err)
  }
})
module.exports = router;