const mongoose = require('mongoose')
const palacesschema = new mongoose.Schema({
    que: String,
    vote: {
        type: [{
            dist: String,
            state: String,
            party:String,
            count: Number
        }]
    },
    totalvote:Array,
    voters: Array,
    options: Array,
    status: {
        type: String,
        enum: ['active', 'deactive'],
        required: true
    }
})
const Palaces = new mongoose.model('Pole', palacesschema)
module.exports = Palaces;