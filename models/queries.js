const mongoose = require('mongoose')
const qeryschema = new mongoose.Schema({
    name: String,
    email:String,
    mobile:String,
    dist:String,
    city:String,
    state:String,
    foradd:String,
    joingroup:String,
    genenquiry:String,
    date:Date
})
const Queries = new mongoose.model('Query', qeryschema)
module.exports = Queries;