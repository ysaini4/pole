const mongoose = require('mongoose')
const Palaces = new mongoose.model('Palace',new mongoose.Schema({
    state:{
        name:{
            type:String,
            unique:true
        },
        dist: [new mongoose.Schema({
            name:{
                type:String
                
            }
        })]
    }
}))
module.exports = Palaces;