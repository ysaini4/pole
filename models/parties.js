const mongoose = require('mongoose')
const Parties = new mongoose.model('Party',new mongoose.Schema({
    name:{
        type:String,
        unique:true
    },
    status:String,
    image:String
}))
module.exports = Parties;